import uk.ac.leedsBeckett.Account

class BootStrap {

    def init = { servletContext ->
    	new Account(number: 123456, balance: 1000.00, pin: '1234').save(flush: true)
    }
    def destroy = {
    }
}
