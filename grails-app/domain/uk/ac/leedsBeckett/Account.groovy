package uk.ac.leedsBeckett


import grails.rest.*

@Resource(readOnly = false, formats = ['json', 'xml'])
class Account {
    Integer number
    Double balance
    String pin
}
